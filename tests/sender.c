#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#define SERVER "127.0.0.1"
#define PORT 9876
#define MAXBLOCK 10240
#define DATACHAR 'Z'
#define NUMBLOCKS 3

int main() {
    int sock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP), opt = 1;
    struct sockaddr_in addr;
    memset(&addr, 0, sizeof(addr));
    addr.sin_family = AF_INET;
    inet_aton(SERVER, &addr.sin_addr);
    addr.sin_port = htons(PORT);
    if(connect(sock, (struct sockaddr *) &addr, sizeof(addr))) {
        printf("connect to %s:%d failed", SERVER, PORT);
        exit(2);
    }
    char buf[MAXBLOCK];
    memset(buf, DATACHAR, MAXBLOCK);
    FILE *stream = fdopen(sock, "w");
    srand(time(NULL));
    for(int i = 0; i < NUMBLOCKS; i++) {
        int n = 1 + rand() % MAXBLOCK;
        printf("Sending %d %c-bytes\n", n, DATACHAR);
        fprintf(stream, "%d\n", n); fflush(stream);
        fwrite(buf, 1, n, stream);
    }
}