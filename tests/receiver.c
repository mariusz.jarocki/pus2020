#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define PORT 9876
#define DATACHAR 'Z'

int main() {
    int sock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP), opt = 1;
    setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, &opt, sizeof(opt));
    struct sockaddr_in addr;
    memset(&addr, 0, sizeof(addr));
    addr.sin_family = AF_INET;
    addr.sin_port = htons(PORT);
    if(bind(sock, (struct sockaddr *) &addr, sizeof(addr))) {
        printf("bind to port %d failed", PORT);
        exit(2);
    }
    listen(sock, 5);
    for(;;) {
        struct sockaddr_in clientaddr;
        unsigned int sockaddr_size = sizeof(clientaddr);
        int csock = accept(sock, (struct sockaddr *) &clientaddr, &sockaddr_size);
        printf("Client from %s:%d connected\n", inet_ntoa(clientaddr.sin_addr), ntohs(clientaddr.sin_port));
        FILE *stream = fdopen(csock, "r");
        for(;;) {
            printf("Reading number of bytes... "); fflush(stdout);
            int n;
            int ok = fscanf(stream, "%d", &n);
            if(ok != 1) {
                printf("end of transmission\n");
                break;
            }
            fgetc(stream); // usuwanie potencjalnego separatora liczba:dane
            printf("%d\n", n);
            while(n > 0) {
                int m = n;
                if(m > 1024) m = 1024; // czytamy porcjami po 1024 bajty
                char buf[1024];
                int l = fread(buf, 1, m, stream);
                if(l <= 0) {
                    printf("\nRead failed\n");
                    break;
                }
                n -= l;
                printf("OK, checking if all data are %c-byte\n", DATACHAR);
                for(int i = 0; i < l; i++) {
                    if(buf[i] != DATACHAR) {
                        printf("buf[%d] = 0x%02X !\n", i, buf[i]);
                    }
                }
            }
        }
    }
}