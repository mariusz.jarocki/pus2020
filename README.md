Projekt kursowy
Programowanie Usług Sieciowych
2020/21

# buforowanie (3-4.5)

* po zalogowaniu uzytkownik dostaje wszystkie wiadomosci, ktore nie zostaly odczytane na tym kliencie

# znajomi (3-4)

* wysylac wiadomosc mozna tylko do tych osob, ktore wczesniej potwierdzily ze chca od nas je otrzymywac

# grupy (3-4.5)

* definiujemy grupe uzytkownikow i mozemy wysylac wiadomosci do wszystkich z grupy

# pliki (4-5)

* uzytkownik moze wyslac plik z lokalnego systemu innemu uzytkownikowi, ktory moze go zapisac w swoim

# wieloserwerowosc (4.5-5)

* dwa (conajmniej) wspolpracujace ze soba serwery, zsynchronizowana baza danych i zmodyfikowana reakcja na polecenia oraz przesylanie wiadomosci